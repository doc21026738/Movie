const jwt = require('jsonwebtoken')

module.exports=(req,res,next)=>{

    req.method==="OPTIONS"?next:null;
    try{
        const token = req.headers.credentials.split(' ')[1]
        if(!token){res.status(401).json({message: "user without credentials"})}
        let decoded = jwt.verify(token,process.env.jwt)
        req.credential = decoded;
        next()
    }catch(e){
        res.json({message: e.message})
    }
}